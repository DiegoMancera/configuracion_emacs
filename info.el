(defvar my/nombre "Usuario")
(defvar my/email "correo@correo.org")

(defvar my/git "https://gitlab.com/UsuarioGit")
(defvar my/user "Nickname")
(defvar my/website "https://website.org")
(defvar my/cloud "https://cloud.website.org")
(defvar my/caldav (format "%s/remote.php/dav/calendars/%s/" my/cloud my/user))

(defvar my/usuario-principal "usuarioprincipal")
(defvar my/home-usuario-principal (format "/home/%s/" my/usuario-principal))

;; Rutas preferidas de org
(defvar my/org (format "/home/%s/.org/" my/usuario-principal))
(defvar my/orgroam (format "%s%s" my/org "OrgRoam/"))
(defvar my/diario-personal (format "%s%s" my/org "SeguimientoPersonal.org"))
(defvar my/diario-empresa (format "%s%s" my/org "SeguimientoLaboral.org"))
(defvar my/agenda-personal (format "%s%s" my/org "AgendaPersonal.org"))
(defvar my/agenda-empresa (format "%s%s" my/org "AgendaLaboral.org"))
(defvar my/notas-personales (format "%s%s" my/org "NotasPersonales.org"))
(defvar my/notas-empresa (format "%s%s" my/org "NotasLaborales.org"))
(defvar my/notas-rapidas (format "%s%s" my/org "Notas/Notas.org"))
(defvar my/contactos (format "%s%s" my/org "Contactos/Contactos.org"))

(defvar my/finanzas-empresa (format "%s%s" my/org "FinanzasLaborales.ledger"))
(defvar my/finanzas-personales (format "%s%s" my/org "FinanzasPesonales.ledger"))

(defvar my/sources "~/Sources")

(provide 'info)
